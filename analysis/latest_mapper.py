#
# MapReduce example.
#
# The aim is to investigate MapReduce approaches to analysing MD trajectory
# data. In this case the trajectory is of 18 base pairs of double-stranded
# DNA. We want to find out the average structure of the trinucleotide
# subsequence AGT. The DNA being examined here has the sequence:
#
# GCTCAGTCAGTCAGTCGC  
#
# so  the subsequence 'AGT' actually occurs three times - at positions
# 5-7, 9-11, and 13-15. Thus each snapshot in the trajectory  file gives us
# three instances of the structure of AGT.
#
# In order to calculate an average structure we have to be sure to first of
# all bring all instances into the same frame of reference (removing
# translational and rotational movements, which are not of interest). We do
# this by least-squares fitting each instance to a reference structure before
# we emit the coordinates.
#

import os

import MDAnalysis as mda
from MDAnalysis.analysis.align import alignto

topfile = '../top/AGTC.pdb'
trjfile = '../nc/AGTC_test1.nc'
reference = 'AGT.pdb'
key = 'AGT'
sequence_length = 18

u = mda.Universe(topfile,trjfile, format='NCDF')

#
# make a sequence string from the residue names
#
seq_string = ''
for r in u.residues[:sequence_length]:
    seq_string += r.name[1]

#
# search for every occurence of the key in seq_string
#
start_pos = 0
key_pos = []
end_of_string = False

while not end_of_string:
    try:
        p = seq_string.index(key,start_pos)
    except ValueError:
        end_of_string = True
    else:
        key_pos.append(p)
        start_pos = p + 1
#
# build a set of selection definitions
#
sel_def = []
for p in key_pos:
    r1 = p + 1
    r2 = r1 + len(key) - 1
    r3 = 2 * sequence_length - r2 + 1
    r4 = r3 + len(key) - 1
    sel_def.append('resid {}-{} or resid {}-{}'.format(r1,r2,r3,r4))

#
# Create selections for each of the sel_defs:
#
sel = []
for sd in sel_def:
    sel.append(u.select_atoms(sd))

#
# Check whether the reference file already exists and if it does not exist then create it.
#
if not os.path.isfile('./'+reference):
    w = u.select_atoms(sel_def[0])
    w.write('./'+reference)

ref = mda.Universe(reference)

#
# now work through the trajectory. As this is just a toy example, just
# print out the x,y, and z coordinates of the first atom.
#
partial_count = 1
for ts in u.trajectory:
    for s in sel:
        alignto(s,ref)
        c = s.coordinates()[0]
        print key, c[0],c[1],c[2], partial_count


