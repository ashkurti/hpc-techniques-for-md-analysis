#
# MapReduce example.
#
# Examining the potential of MapReduce to analyse MD trajectory data.
#
# Here we have a simple Reduce function. The input is a key (a DNA sequence)
# and a value, which actually has two components: (coordinates, partial_count).
# Coordinates is just the x,y, and z coordinates of the first atom in the key
# sequence (this is just a toy exampple - in reality it should be all of them),
# and partial_count is the number of instances that the coordinates are the
# average value of.
# The reduce function emits a singe (key, value) pair.

import sys

input = sys.argv[1]

xout = 0.0
yout = 0.0
zout = 0.0
countout = 0
with open(input,'r') as f:
    for line in f:
        s = line.split()
        key = s[0]
        x = float(s[1])
        y = float(s[2])
        z = float(s[3])
        partial_count = int(s[4])

        xout = xout + x * partial_count
        yout = yout + y * partial_count
        zout = zout + z * partial_count
        countout += partial_count

print key, xout/countout, yout/countout, zout/countout, countout
        
