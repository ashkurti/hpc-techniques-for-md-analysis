# README #

The source code in this repository is organised in the following folders:

* analysis
 
Containing files *latest_mapper.py* and *latest_reducer.py* that will serve as a basis to build optimised and HPC-directed code  for the analysis of DNA data. Each of these files has some comments at the top that briefly explain the utility and the logics behind each file.

The workflow can be executed as follows: 
```
python latest_mapper.py > out_mapper.dat
python latest_reducer.py out_mapper.dat > out_reducer.dat
```
The output of each step should be identical (except for machine-related precision errors) to respectively *out_mapper_25Jan16.dat* and *out_reducer_25Jan16.dat*
To compare your results with the provided files please do the following:
```
diff out_mapper.dat out_mapper_25Jan16.dat
diff out_reducer.dat out_reducer_25Jan16.dat
```

* nc

This is a folder that contains the data to be analysed. Currently it contains a file named *AGTC_test1.nc*. This is a DNA trajectory file containing 18 base pairs of double-stranded DNA. We want to find out the average structure of the trinucleotide subsequence AGT (as shown at line 30 of file *analysis/latest_mapper.py* https://bitbucket.org/ashkurti/hpc-techniques-for-md-analysis/src/764a2804a2b212bb03778efb0c81550e4ef5191f/analysis/latest_mapper.py?at=master&fileviewer=file-view-default#latest_mapper.py-30) 
The DNA being examined in this file has the sequence: *GCTCAGTCAGTCAGTCGC* for each snapshot. 

* top

This folder contains a pdb file containing position coordinates of a snapshot of the trajectory file *nc/AGTC_test1.nc* that can be used as a reference file. In fact the reader of a biomolecular simulation trajectory used here, requires a reference file in addition to the trajectory file, to properly read the data in the trajectory file.
  
* additional_data

The data in this folder can be used to examine the achieved parallelism of the optimisation strategies.

### Who do I talk to? ###

* TEY Kai yik, EPCC, s1565434@sms.ed.ac.uk
* Amrey Krause, EPCC - akrause@staffmail.ed.ac.uk
* Iain Bethune, EPCC - ibethune@epcc.ed.ac.uk
* Charlie Laughton, University of Nottingham - charles.laughton@nottingham.ac.uk
* Shantenu Jha, Rutgers University - shantenu.jha@rutgers.edu
* Ioannis Paraskevakos, Rutgers Unviersity - ioannis.paraskevakos@rutgers.edu
* Ardita Shkurti, University of Nottingham - ardita.shkurti@gmail.com